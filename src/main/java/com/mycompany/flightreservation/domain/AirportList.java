/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain;

/**
 *
 * @author ACER
 */
public class AirportList {
    private String airline;
    private String date;
    private String takeOf;
    private String landing;
    private String stops;
    private String price;
    public AirportList(String airline,String date,String takeOf,String landing,String stops,String price){
     this.airline=airline;
     this.date=date;
     this.takeOf=takeOf;
     this.landing=landing;
     this.stops=stops;
     this.price=price;
    }
    public String getairline(){
        return airline;
    }
    public String getdate(){
        return date ;
    }
    public String gettakeOf(){
        return takeOf;
    }
    public String getlanding(){
        return landing;
    }
    public String getstops(){
        return stops;
    }
    public String getprice(){
        return price;
    }
    
}
