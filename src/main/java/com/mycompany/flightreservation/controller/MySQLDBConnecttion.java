/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Room107
 */
public class MySQLDBConnecttion implements DBConnection{
    private String host;
    private String username;
    private String password;
    private String port;
    private Connection connection;
    
    public MySQLDBConnecttion(String host,String dbName,String user,String password,String port){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://" + host + ":" + port + "/" + dbName;
            
          connection = DriverManager.getConnection(url, user, password);
          
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MySQLDBConnecttion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MySQLDBConnecttion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ResultSet executeQuery(String query) {
        Statement st;
        try {
            st = connection.createStatement();
  
             ResultSet results = st.executeQuery(query);
             return results;
        } catch (SQLException ex) {
            Logger.getLogger(MySQLDBConnecttion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }     
   
    }  
    

    @Override
    public void update(String command) {
       Statement st;
        try {
            st = connection.createStatement();
            st.executeUpdate(command);
        } catch (SQLException ex) {
            Logger.getLogger(MySQLDBConnecttion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void close() {
       if(connection!=null){
           try {
               connection.close();
           } catch (SQLException ex) {
               Logger.getLogger(MySQLDBConnecttion.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
    }
}
